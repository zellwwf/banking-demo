# Represents money sources, income from job, bank credits, credit notes.. etc
class Creditor < ApplicationRecord
  has_many :transactions, as: :from

  validates_presence_of :name

  # Creditors have infinite balance for this example's sake.
  def balance
    Float::INFINITY
  end
end
