class User < ApplicationRecord
  has_secure_password
  has_one :account

  validates_presence_of :first_name, :last_name, :password_digest, :email
  validates :email, uniqueness: true, format: URI::MailTo::EMAIL_REGEXP

  delegate :balance, :sent_transactions, :received_transactions, to: :account

  def name
    "#{first_name} #{last_name}"
  end
end
