# Represents money sinks, food, rent, ram, and partying!
class Debitor < ApplicationRecord
  has_many :transactions, as: :to

  validates_presence_of :name
end
