class Account < ApplicationRecord
  belongs_to :user
  has_many :sent_transactions, class_name: 'Transaction', as: :from
  has_many :received_transactions, class_name: 'Transaction', as: :to

  delegate :name, to: :user

  def balance
    sent = sent_transactions.pluck(:amount).reduce(0, :+)
    received = received_transactions.pluck(:amount).reduce(0, :+)

    received - sent
  end

  def add_credit(amount, from = 'Major Bank')
    creditor = Creditor.find_or_create_by(name: from)
    Transaction.create(amount: amount, from: creditor, to: self, created_at: DateTime.now)
  end

  def remove_credit(amount, to = 'Humble Bundle')
    debitor = Debitor.find_or_create_by(name: to)
    Transaction.create(amount: amount, to: debitor, from: self, created_at: DateTime.now)
  end

  def self.transfer(amount, from:, to:)
    raise ArgumentError unless from && to

    Transaction.create(amount: amount, from: from, to: to)
  end
end
