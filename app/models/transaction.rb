class Transaction < ApplicationRecord
  belongs_to :from, polymorphic: true
  belongs_to :to, polymorphic: true

  validates_numericality_of :amount, greater_than: 0, less_than: BigDecimal(10e8, 2)

  validate :source_has_enough_balance, if: -> { from.present? }

  before_validation :ensure_only_two_digits

  def ensure_only_two_digits
    self.amount = BigDecimal(amount, 2)
  end

  def source_has_enough_balance
    raise NotImplementedError unless from.respond_to?(:balance)

    errors.add :amount, 'Source has insufficient balance' unless from.balance >= amount
  end
end
