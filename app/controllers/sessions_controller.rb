class SessionsController < ApplicationController

  def new
    redirect_to root_path, notice: 'You are already signed in' if current_user.present?
  end

  def create
    user = User.find_by_email(session_params[:email])

    if user&.authenticate(session_params[:password])
      session[:current_user_id] = user.id
      redirect_to root_path, notice: "Welcome #{user.last_name}, #{user.first_name}"
    else
      redirect_to login_path, alert: "Woah.. that's not the correct stuff buddy"
    end
  end

  def destroy
    reset_session
    redirect_to root_path
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end
end
