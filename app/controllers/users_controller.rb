class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    return redirect_to :new, alert: "Please Confirm Your Password!" unless password_confirmed?

    @user = User.new(user_params.except(:password_confirm))
    return redirect_to :new, alert: "Fill in everything please" unless @user.valid?

    @user.save
    @user.build_account.save
    @user.account.add_credit(100, 'ZELL BANK')

    redirect_to root_path, notice: 'Account Created!'
  end

  private

  def password_confirmed?
    params[:user][:password] == params[:user][:password_confirm]
  end

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirm)
  end
end
