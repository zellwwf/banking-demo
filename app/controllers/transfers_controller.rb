class TransfersController < ApplicationController
  before_action :authorize!

  def home
    @received_txs = current_user.received_transactions
    @sent_txs = current_user.sent_transactions
    @debitors = Debitor.all
    @friends = User.all.to_a - [current_user]
  end

  def send_money
    tx = Transaction.new(transfer_params)
    if tx.save
      redirect_to root_path, notice: 'Sent Successfully'
    else
      redirect_to root_path, alert: 'Something went wrong!'
    end
  end

  private

  def transfer_params
    params.require(:transaction).permit(:from_type, :to_type, :from_id, :to_id, :amount)
  end
end
