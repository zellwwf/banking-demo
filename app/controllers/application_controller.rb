class ApplicationController < ActionController::Base

  private

  def current_user
    return false unless session[:current_user_id]

    @current_user ||= User.find(session[:current_user_id])
  rescue ActiveRecord::RecordNotFound
    false
  end

  def authorize!
    redirect_to login_path unless current_user
  end

  helper_method :current_user
end
