# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

user = User.create(first_name: 'Abdulilah', last_name: 'Azzazi', email: 'abd@gmail.com', password: 'password1234')
account = user.create_account!
account.add_credit('100')

user = User.create(first_name: 'Bob', last_name: 'Loblaw', email: 'bob@gmail.com', password: 'password1234')
account = user.create_account!
account.add_credit('100')

user = User.create(first_name: 'Willy', last_name: 'Wonka', email: 'willy@gmail.com', password: 'password1234')
account = user.create_account!
account.add_credit('100')

Debitor.create(name: 'Utilities Bill')
Debitor.create(name: 'Groceries')
Debitor.create(name: 'Parties')