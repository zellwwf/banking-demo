class CreateDebitors < ActiveRecord::Migration[7.0]
  def change
    create_table :debitors do |t|
      t.string :name

      t.timestamps
    end
  end
end
