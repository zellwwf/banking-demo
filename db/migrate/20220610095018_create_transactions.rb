class CreateTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :transactions do |t|
      t.references :from, polymorphic: true, null: false
      t.references :to, polymorphic: true, null: false
      t.decimal :amount, precision: 12, scale: 3

      t.timestamp :created_at
    end

    add_index :transactions, :created_at
  end
end
