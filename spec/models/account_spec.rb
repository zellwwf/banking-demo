# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Account, type: :model do
  describe '#add_credit' do
    let(:user) { FactoryBot.create :user }
    let(:account) { Account.create(user: user) }

    context 'with valid amounts' do
      let(:credits) { [rand(10e7)] * 10 }

      it 'adds credits and correctly updates the balance' do
        sum = BigDecimal(credits.sum)
        credits.each { |amount| account.add_credit(amount) }

        expect(account.balance).to eq(sum)
        expect(account.received_transactions.size).to eq(10)
      end
    end
  end


  describe '#remove_credit' do
    let(:user) { FactoryBot.create :user }
    let(:account) { Account.create(user: user) }

    it 'removes credits and correctly updates the balance' do
      account.add_credit(10)
      account.remove_credit(10)

      expect(account.balance).to eq(0)
      expect(account.received_transactions.size).to eq(1)
      expect(account.sent_transactions.size).to eq(1)
    end

    it 'cannot remove more than the user has' do
      account.add_credit(10)
      account.remove_credit(100)

      expect(account.balance).to eq(10)
      expect(account.received_transactions.size).to eq(1)
      expect(account.sent_transactions.size).to eq(0)
    end
  end

  describe '#transfer' do
    let(:sending_account) { Account.create(user: FactoryBot.create(:user)) }
    let(:receiving_account) { Account.create(user: FactoryBot.create(:user)) }
    let(:base_balance) { 100 }
    let(:transferred_amount) { base_balance / 2 }

    before do
      receiving_account.add_credit(base_balance)
      sending_account.add_credit(base_balance)

      Account.transfer transferred_amount, from: sending_account, to: receiving_account
    end

    it 'should decrease the sending accounts balance' do
      expect(sending_account.balance).to eq(base_balance - transferred_amount)
    end

    it 'should increase the receivers balance' do
      expect(receiving_account.balance).to eq(base_balance + transferred_amount)
    end
  end
end
