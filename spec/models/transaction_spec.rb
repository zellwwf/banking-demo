require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe '#create method' do
    let(:sending_account) { Account.create(user: FactoryBot.create(:user)) }
    let(:poor_man_account) { Account.create(user: FactoryBot.create(:user)) }
    let(:receiving_account) { Account.create(user: FactoryBot.create(:user)) }

    before do
      sending_account.add_credit(0.999e9 - 1)
      poor_man_account.add_credit(10)
    end

    context 'without valid sending / receiving accounts' do
      it 'does not create a tx without a valid accounts' do
        subject.amount = 100

        subject.save

        expect(subject.errors[:from].first).to eq 'must exist'
        expect(subject.errors[:to].first).to eq 'must exist'
      end

      it 'does not create a transaction if sending account has not enough balance' do
        subject.from = poor_man_account
        subject.to = receiving_account

        subject.amount = 100

        subject.save
        expect(subject.errors[:amount].first).to eq 'Source has insufficient balance'
      end
    end

    context 'with invalid amounts' do
      before do
        subject.from = sending_account
        subject.to = receiving_account
      end

      it 'does not allow negative numbers' do
        random_neg = - rand(1000)
        subject.amount = random_neg

        result = subject.save

        expect(subject.errors[:amount].first).to eq 'must be greater than 0'
        expect(result).to be false
      end

      it 'does not allow zeros' do
        subject.amount = 0

        result = subject.save

        expect(subject.errors[:amount].first).to eq 'must be greater than 0'
        expect(result).to be false
      end

      it 'does not allow numbers above or equal 10e8' do
        subject.amount = 10e8

        result = subject.save

        expect(subject.errors[:amount].first).to eq 'must be less than 1000000000.0'
        expect(result).to be false
      end

      it 'does not allow numbers with over 3 decimal places' do
        subject.amount = BigDecimal(0.0001, 2)

        result = subject.save
        expect(result).to be false
      end
    end

    context 'with valid amounts' do
      before do
        subject.from = sending_account
        subject.to = receiving_account
      end

      it 'allows 0.001 in (0, 0.999e9)' do
        subject.amount = 0.001

        result = subject.valid?
        expect(result).to be true
      end

      it 'allows 0.990e9 boundary points in (0, 0.999e9)' do
        subject.amount = BigDecimal(0.990e9, 2)

        result = subject.valid?
        expect(result).to be true
      end

      it 'allows amounts in (0, 0.999e9) ' do
        subject.amount = rand(0.990e9)

        result = subject.valid?
        expect(result).to be true
      end
    end
  end
end
