require 'rails_helper'

RSpec.describe Debitor, type: :model do
  it 'saves with any name thats not empty' do
    subject.name = 'hello'

    expect(subject.valid?).to be true
  end

  it 'does not save without a name' do
    subject.name = ''

    expect(subject.valid?).to be false
  end
end
