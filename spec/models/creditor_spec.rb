require 'rails_helper'

RSpec.describe Creditor, type: :model do
  it 'saves with any name thats not empty' do
    subject.name = 'hello'

    expect(subject.valid?).to be true
  end

  it 'does not save without a name' do
    subject.name = ''

    expect(subject.valid?).to be false
  end

  it 'defines an infinite balance (for our example)' do
    expect(subject.send(:balance)).to eq(Float::INFINITY)
 end
end
