require 'rails_helper'

RSpec.describe User, type: :model do
  context 'when the user has invalid data' do
    it 'fails validation without an email, first_name, last_name or password' do
      subject.valid?

      expect(subject.errors[:first_name].first).to eq "can't be blank"
      expect(subject.errors[:last_name].first).to eq "can't be blank"
      expect(subject.errors[:password_digest].first).to eq "can't be blank"
      expect(subject.errors[:email]).to eq(["can't be blank", 'is invalid'])
    end

    it 'fails when email is not the correct format' do
      subject.email = 'zellwwf'
      subject.valid?

      expect(subject.errors[:email]).to eq(['is invalid'])
    end
  end

  context 'when the user has good data' do
    let(:user) { FactoryBot.create :user }

    it 'allows validations to pass' do
      expect(user.valid?).to be true
    end
  end

  context 'Authentication' do
    let(:user) { FactoryBot.create :user }

    it 'does not authenticate for wrong passwords' do
      expect(user.authenticate('hello world')).to be false
    end

    it 'allows validations to pass' do
      expect(user.authenticate('somepassword')).to eq user
    end
  end
end
