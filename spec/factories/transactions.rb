FactoryBot.define do
  factory :transaction do
    from { nil }
    to { nil }
    amount { "9.99" }
    created_at { "2022-06-10 11:50:18" }
  end
end

